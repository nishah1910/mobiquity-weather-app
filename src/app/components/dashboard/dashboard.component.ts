import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../../services/weather.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  panelOpenState = false;
  cityList = ['London', 'Berlin', 'Madrid', 'Paris', 'Budapest']
  cityDetails = [];

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.getCityData();
  }

  getCityData() {
    this.cityList.forEach((city) => {
      let details = {};
      this.weatherService.getCityWeatherData(city).subscribe((response: any) => {
        details['weather'] = response;
      });
      this.weatherService.getCityForecastData(city).subscribe((response: any) => {
        details['forecast'] = response;
      });
      this.cityDetails.push(details);
    });
    return this.cityDetails;
  }

}
