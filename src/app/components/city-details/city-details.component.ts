import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-city-details',
  templateUrl: './city-details.component.html',
  styleUrls: ['./city-details.component.scss']
})
export class CityDetailsComponent implements OnInit, OnChanges {
  @Input() cityDetails;
  filteredCityDetails = [];

  constructor() { }

  ngOnChanges() {
    if(!this.cityDetails){
      return;
    }
    this.filteredCityDetails = this.cityDetails.list.filter(({dt_txt}) => dt_txt.match(/^.+? (09:00:00)$/g));
  }

  ngOnInit() {}

}
