import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  API_ID = environment.apiId;

  constructor(private httpclient: HttpClient) { }

  public getCityWeatherData(city: string) {
    const endpoint = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${this.API_ID}`;
    return this.httpclient.get(endpoint);
  }

  public getCityForecastData(city: string) {
    const endpoint = `http://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${this.API_ID}`;
    return this.httpclient.get(endpoint);
  }
}
